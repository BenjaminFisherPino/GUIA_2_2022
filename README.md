# GUIA 1 2022

Guía número 2 sistema y redes 2022


## Resúmen del programa

Estos programas, desarrollados por Benjamín Fisher, nos permiten determinar la cantidad de lineas y caracteres de x cantidad de archivos de texto. El primero realiza el calculo de forma secuencial (un archivo y luego el siguiente), mientras que el segundo utiliza threads para analizar los archivos simultaneamente.

## Requerimientos

Para el correcto funcionamiento de los programas se requiere tener instalado  make en su version 4.2.1 y un compilador g++. En caso de no tener uno de estos programas el codigo no funcionara.

Si desea comprobar la version de sus programas, ingrese la siguiente linea de codigo en su terminal.

```
(programa) --version
```

En caso de tener otra version, actualice sus programas.

Si no sabe como instalar los programas, ingrese la siguiente linea de codigo en su terminal.

```
sudo apt install (programa)
```

## Instalacion

Para descargar los programa hay dos opciones. Usted puede clonar el repositorio en su computador mediante la utilizacion de git, o, puede descargar manualmente la carpeta.

Una vez descargado la carpeta, usted debe dirigirse a la ubicacion donde se alojan los programas. Luego debera escoger un programa, en la carpeta EJ_1 se encuentra el programa 1 y en EJ_2 el programa 2. Finalmente debera escribir el siguiente comando.

Para el primer programa escriba:

```
make
```

Para el segundo programa escriba:

```
g++ Main.cpp Calculo.cpp -o proyecto -lpthread
```

Estos comandos nos permiten compilar nuestro programa y generar un ejecutable. Finalmente usted debera ingresar el siguiente comando en su terminal para ejecutar el programa.


```
./programa N
```

N es el nombre del archivo de texto que sera analizado. Usted puede ingresar mas de un N para la ejecucion del programa.

```
./programa N1 N2 N3 N4 .... NX
```

## Funcionamiento del programa

### Programa 1

El programa 1 nos permite contar la cantidad de lineas y caracteres de archivos de texto de forma securncial. Mediante un ciclo, que comienza en la línea 28, se llama a una funcion y como parametro el nombre de un archivo que ingreso el usuario. En esta funcion (procesar_archivo()) se abre el archivo en cuestion, se recorre y se calcula el total de lineas y caracteres. Un detalle importante es que antes de comenzar el ciclo descrito anteriormente, se setea un tiempo de inicio y finalizando el ciclo otro. Al final de la ejecucion se saca la diferencia entre ambos tiempos para sacar el tiempo de ejecucion total.

### Programa 2

El programa 2 realiza lo mismo que el 1, pero se implementan threads para analizar los archivos simultaneamente. La <pthread.h> de c++ fue utilizada para la creacion y manejo de threads para el analisis simultaneo de los archivos de texto. El codigo tiene 2 ciclos, el primero (linea 41), nos permite la creacion de los threads y la asignacion de un archivo de texto a cada uno y el segundo (linea 46) espera a que termine el proceso para eliminar los threads. Al igual que en el programa anterior, se toma el tiempo al iniciar el proceso y al finalizar.

## Conclusion

Tras correr el programa con archivos de texto de distintos tamaños pude sacar un par de conclusiones.

1) Si vamos a utilizar archivos relativamente pequeños nos sale mas acuenta el primer programa, ya que, al ser poco nuestra computadora puede realizar los calculos sin mayor dificultad, ademas, nos ahorramos la implementacion de los threads que es un poco confusa.

2) Si vamos a utilizar archivos grandes es mejor el segundo programa, ya que, analiza todos los archivos simultaneamente en procesos distintos e independientes entre si y lo mas importante, es mas eficiente y rapido.

## Agradecimiento

Gracias por utilizar este programa!


## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca.

Contacto
→ Mail: bfisher20@alumnos.utalca.cl

