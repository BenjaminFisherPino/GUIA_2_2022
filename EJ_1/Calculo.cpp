#include <iostream>
#include <fstream>
#include <string>
#include "Calculo.h"

using namespace std;

Calculo::Calculo(){
    this->total_char = 0;
    this->total_lines = 0;
    this->total_words = 0;
}

void Calculo::words(char *archivo){ // modificar para que no cuente espacios y cuente palabras

    ifstream file(archivo);
    int word = 0; //will not count first word so initial value is 1
    char ch;
    file.seekg(0,ios::beg); //bring position of file pointer to begining of file
 
    while(file){
        file.get(ch);
        if(ch == ' ' || ch == '\n'){

            word++; // it counts empty lines too, must modify.
        }      
    } 

    file.close();
    cout << "cantidad de palabras: " << word-1 << endl;
    this->total_words = this->total_words + word-1;
}

void Calculo::chars(char *archivo){

	ifstream file(archivo);
	char ch;
    int chars = 0;

    while(file){
        file.get(ch);
        if(ch==' '||ch=='\n'){
        } else{
            chars++;
        }   
    } 

    file.close();	
    cout << "cantidad de caracteres: " << chars << endl << endl;
    this->total_char = this->total_char + chars;
}

void Calculo::lines(char *archivo){

    ifstream file(archivo);
    string str,file_name = archivo;
    int lines = 0;
    while (std::getline(file,str)){
        lines++;
    }

    file.close();
    cout << "Archivo: " << file_name << endl;
    cout << "Cantidad de lineas: " << lines << endl;
    this->total_lines = this->total_lines + lines;

}

int Calculo::get_total_char(){
    return this->total_char;
}

int Calculo::get_total_lines(){
    return this->total_lines;
}

int Calculo::get_total_words(){
    return this->total_words;
}
