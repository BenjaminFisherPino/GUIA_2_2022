#include <unistd.h>
#include <string.h>
#include <iostream>
#include <chrono>

#include "Calculo.h"
using namespace std;

/*MAIN FUNCTION*/
int main (int argc, char *argv[]){

    system("clear");
    
    Calculo c;
    srand((unsigned) time(0));

    /*MAIN CYCLE*/
    auto start_s = chrono::steady_clock::now(); // Time starts
    for (int i=1;i<argc;i++){
        c.lines(argv[i]);
        c.words(argv[i]);
        c.chars(argv[i]);      
    }

    auto end_s = std::chrono::steady_clock::now(); // Time Finishes
    chrono::duration<double> elapsed_seconds_s = end_s - start_s; // Time difference
    cout << endl <<"Tiempo ejercicio 1: " << elapsed_seconds_s.count()  << " segundos" << endl;

    cout << "Total de caracteres: " << c.get_total_char() << endl;
    cout << "Total de lineas: " << c.get_total_lines() << endl;
    cout << "Total de palabras: " << c.get_total_words() << endl;

    return 0;
}