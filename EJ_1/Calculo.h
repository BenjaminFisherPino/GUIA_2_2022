#ifndef CALCULO_H
#define CALCULO_H

class Calculo {

    /*ATRIBUTOS*/
    private:
      int total_char, total_words, total_lines;
      
    public:

    /*CONSTRUCTOR*/
		Calculo();

    /*METODOS*/
        void words(char *archivo);
        void chars(char * archivo);
        void lines(char *archivo);
        int get_total_char();
        int get_total_words();
        int get_total_lines();
};
#endif
