#include <pthread.h>
#include <iostream>
#include <chrono>
#include "Calculo.h"

using namespace std;

/*EXTERNAL FUNCTION*/
void *archivo(void *arg){ //modificar esta wea
    Calculo c = Calculo();
    char *name = (char*)arg;
    c.procesar_archivo(name);
    pthread_exit(0);
}

/*MAIN FUNCTION*/
int main(int argc, char *argv[]) {

    system("clear");
    
    // Thread ID
    pthread_t tid[argc-1];
  
    // Thread variables
    pthread_attr_t attr; 

    srand((unsigned) time(0));

    /*Checking the parameters*/
    if (argc <= 1) {
        cout << "Debe ingresar al menos un archivo de texto" << endl;
        return -1;
    }

    pthread_attr_init (&attr);

    auto start_s = chrono::steady_clock::now(); // Time starts

    // Create the threads
    for (int i = 1 ; i < argc ; i++){
        pthread_create (&tid[i], &attr, archivo , argv[i]);
    }

    // Waits until all the threads finish
    for (int i = 1; i < argc;i++){
        pthread_join(tid[i],NULL);
    }

    auto end_s = std::chrono::steady_clock::now(); // Time Finishes
    chrono::duration<double> elapsed_seconds_s = end_s - start_s; // Time difference

    cout << endl <<"Tiempo ejercicio 2: " << elapsed_seconds_s.count()  << " segundos" << endl << endl;
    //cout << "Total de caracteres: " << c.get_total_char() << endl;
    //cout << "Total de lineas: " << c.get_total_lines() << endl;
    //cout << "Total de palabras: " << c.get_total_words() << endl;
}